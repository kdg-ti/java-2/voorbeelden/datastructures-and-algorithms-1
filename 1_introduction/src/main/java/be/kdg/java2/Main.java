package be.kdg.java2;

import java.util.Random;

public class Main {
    private static long stepCounter = 0;

    public static void selectionSort(int[] array) {
        stepCounter = 0;
        for (int i = 0; i < array.length - 1; i++) {
            int indexSmallest = i;
            for (int j = i + 1; j < array.length; j++) {
                stepCounter++;
                if (array[j] < array[indexSmallest]) {
                    indexSmallest = j;
                }
            }
            int tmp = array[i];
            array[i] = array[indexSmallest];
            array[indexSmallest] = tmp;
        }
    }

    public static void randomSort(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length * array.length * array.length; i++) {
            int randomIndex1 = random.nextInt(array.length - 1);
            int randomIndex2 = random.nextInt(array.length - randomIndex1) + randomIndex1;
            if (array[randomIndex1] > array[randomIndex2]) {
                int tmp = array[randomIndex1];
                array[randomIndex1] = array[randomIndex2];
                array[randomIndex2] = tmp;
            }
        }
    }

    public static void timeSelectionSort(int arraySize) {
        int[] numbers = new Random().ints(arraySize).toArray();
        long startTime = System.currentTimeMillis();
        selectionSort(numbers);
        long duration = System.currentTimeMillis() - startTime;
        System.out.printf("Sorting array of n = %d took %d milliseconds\n", arraySize, duration);
        System.out.printf("Sorting array of n = %d took %d steps...\n", arraySize, stepCounter);
    }

    public static boolean contains(int[] array, int number) {
        for (int j : array) {
            if (j == number) return true;
        }
        return false;
    }

    public static double average(int[] array){
        double total = 0;
        for (int i : array) {
            total += i;
        }
        return total/array.length;
    }

    public static void main(String[] args) {
        for (int n=10000;n<=50000;n+=10000) {
            timeSelectionSort(n);
        }
    }
}
