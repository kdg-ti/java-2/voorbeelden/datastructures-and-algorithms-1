package be.kdg.java2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static void sayHello(int n) {
        if (n <= 0) return;
        System.out.println("Hello!" + n);
        sayHello(n - 1);
    }

    public static long factorial(int n) {
        if (n < 0) throw new IllegalArgumentException();
        if (n == 0) return 1;
        return n * factorial(n - 1);
    }

    public static void rec(int n) {
        if (n == 0) return;
        rec(n - 1);
        System.out.println(n);
        rec(n - 1);
    }

    //OPGELET: Aangezien we een JavaFX applicatie runnen (voor Sierpinski) moet je opstarten via het Gradle-
    //         toolvenster of van de command prompt (gradlew 2_recursion:run). Anders vindt hij de javafx module niet...!
    public static void main(String[] args) {
       sayHello(5);
       System.out.println("5! =  " + factorial(5));
       rec(3);
       System.out.println("GGD(270,192)=" + new Euclides().calculateGCD(270, 192));
       launch(args);
       new Hanoi().moveTower(6, 1, 2, 3);
       factorial(1000);//TODO: tot hoever kan je gaan voordat je een StackOverFlowException krijgt?
       System.out.println("Ready!");
    }

   // @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(new Sierpinski()));
        primaryStage.show();
    }
}
