package be.kdg.java2;

import be.kdg.java2.kollections.ArrayList;
import be.kdg.java2.kollections.Kollections;
import be.kdg.java2.kollections.LinkedList;
import be.kdg.java2.kollections.List;

import java.util.Random;

public class PerformanceTester {

    //TODO: change this method for own use
    public static List<Student> randomList(int n) {
        List<Student> myList = new ArrayList<>();
        new Random().ints(n).forEach(i -> myList.add(new Student(i)));
        return myList;
    }

    public static void compareArrayListAndLinkedList(int n) {
        //add at beginning
        //TODO: use System.currentTimeMillis to compare performance of ArrayList en LinkedList
        //get at end
        //TODO: use System.currentTimeMillis to compare performance of ArrayList en LinkedList
    }

    public static void testSelectionSort() {
        //TODO: test selectionsort for (int n = 1000; n < 20000; n += 1000)
    }

    public static void testMergeSort() {
        //TODO: test mergesort for (int n = 1000; n < 200000; n += 1000)
    }
}
